<?php return array(
    'root' => array(
        'pretty_version' => 'dev-main',
        'version' => 'dev-main',
        'type' => 'library',
        'install_path' => __DIR__ . '/../../',
        'aliases' => array(),
        'reference' => '4a41b722bd2dc2fe5af0b052a914347e3556593e',
        'name' => '__root__',
        'dev' => true,
    ),
    'versions' => array(
        '__root__' => array(
            'pretty_version' => 'dev-main',
            'version' => 'dev-main',
            'type' => 'library',
            'install_path' => __DIR__ . '/../../',
            'aliases' => array(),
            'reference' => '4a41b722bd2dc2fe5af0b052a914347e3556593e',
            'dev_requirement' => false,
        ),
        'thingengineer/mysqli-database-class' => array(
            'pretty_version' => 'dev-master',
            'version' => 'dev-master',
            'type' => 'library',
            'install_path' => __DIR__ . '/../thingengineer/mysqli-database-class',
            'aliases' => array(
                0 => '9999999-dev',
            ),
            'reference' => '5159467ae081adbe96586e45e65a58c0fe7a32ce',
            'dev_requirement' => false,
        ),
    ),
);
